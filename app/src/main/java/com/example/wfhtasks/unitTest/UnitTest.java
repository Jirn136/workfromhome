package com.example.wfhtasks.unitTest;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.wfhtasks.R;

public class UnitTest extends AppCompatActivity {
    private static final String TAG = "Activity";
    private Calculator calculator;
    private EditText edtFirst;
    private EditText edtSecond;
    private TextView txtAns;

    private static Double getOperand(EditText operandEditText) {
        String operandText = getOperandText( operandEditText );
        return Double.valueOf( operandText );
    }

    private static String getOperandText(EditText operandEditText) {
        String operandText = operandEditText.getText().toString();
        if (TextUtils.isEmpty( operandText )) {
            throw new NumberFormatException( "Operand cannot be empty!" );
        }
        return operandText;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_unit_test );

        calculator = new Calculator();

        edtFirst = findViewById( R.id.operand_one_edit_text );
        edtSecond = findViewById( R.id.operand_two_edit_text );
        txtAns = findViewById( R.id.operation_result_text_view );

    }

    public void onMul(View view) {
    }

    public void onDiv(View view) {
        try {
            compute( Calculator.Operator.ADD );
        } catch (IllegalArgumentException e) {
            assert e.getMessage() != null;
            Log.e( TAG, e.getMessage() );
            txtAns.setText( getString( R.string.computationError ) );
        }
    }

    public void onSub(View view) {
        compute( Calculator.Operator.ADD );
    }

    public void onAdd(View view) {
        compute( Calculator.Operator.ADD );
    }

    private void compute(Calculator.Operator operator) {
        double operandOne = 0;
        double operandTwo = 0;
        try {
            operandOne = getOperand( edtFirst );
            operandTwo = getOperand( edtSecond );
        } catch (NumberFormatException e) {
            Log.e( TAG, "Number Format Exception : " + e.getMessage() );
        }
        String result;

        switch (operator) {
            case ADD:
                result = String.valueOf( calculator.add( operandOne, operandTwo ) );
                break;
            case SUB:
                result = String.valueOf( calculator.sub( operandOne, operandTwo ) );
                break;
            case DIV:
                result = String.valueOf( calculator.div( operandOne, operandTwo ) );
                break;
            case MUL:
                result = String.valueOf( calculator.mul( operandOne, operandTwo ) );
                break;
            default:
                result = getString( R.string.computationError );
                break;
        }
        txtAns.setText( result );
    }
}
