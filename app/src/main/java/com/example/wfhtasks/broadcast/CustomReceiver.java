package com.example.wfhtasks.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.example.wfhtasks.BuildConfig;
import com.example.wfhtasks.R;

public class CustomReceiver extends BroadcastReceiver {
    private static final String CUSTOM_CONTENT = BuildConfig.APPLICATION_ID + ".CUSTOM_BROADCAST";

    @Override
    public void onReceive(Context context, Intent intent) {
        String intentAction = intent.getAction();

        if (intentAction != null) {
            String toast = context.getString(R.string.unknown_action);
            switch (intentAction) {
                case Intent.ACTION_POWER_CONNECTED:
                    toast = context.getString(R.string.power_connected);
                    break;
                case Intent.ACTION_POWER_DISCONNECTED:
                    toast = context.getString(R.string.power_disconnected);
                    break;
            }
            Toast.makeText(context, toast, Toast.LENGTH_SHORT).show();
        }


    }
}
