package com.example.wfhtasks.broadcast;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.wfhtasks.BuildConfig;
import com.example.wfhtasks.R;

public class BroadcastActivity extends AppCompatActivity {
    private static final String CUSTOM_CONTENT = BuildConfig.APPLICATION_ID + ".CUSTOM_BROADCAST";
    private CustomReceiver receiver = new CustomReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcast);

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_POWER_CONNECTED);
        filter.addAction(Intent.ACTION_POWER_DISCONNECTED);

        this.registerReceiver(receiver, filter);

        LocalBroadcastManager.getInstance(this).registerReceiver
                (receiver, new IntentFilter(CUSTOM_CONTENT));
    }

    public void sendCustomBroadcast(View view) {
        Intent customIntent = new Intent(CUSTOM_CONTENT);
        LocalBroadcastManager.getInstance(this).sendBroadcast(customIntent);
    }

    @Override
    protected void onDestroy() {
        this.unregisterReceiver(receiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onDestroy();
    }
}
