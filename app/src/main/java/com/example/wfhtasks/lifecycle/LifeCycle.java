package com.example.wfhtasks.lifecycle;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.example.wfhtasks.R;

public class LifeCycle extends AppCompatActivity {
    private static final String TAG = "LifeCycle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_cycle);
        Log.i(TAG, "OnCreate done");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "OnPause done");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "OnResume done");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "OnRestart done");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "OnDestroy done");
    }

    
    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "OnStart done");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "OnStop done");
    }
}
