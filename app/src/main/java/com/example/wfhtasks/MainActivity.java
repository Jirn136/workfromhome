package com.example.wfhtasks;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.wfhtasks.asynctask.AsyncActivity;
import com.example.wfhtasks.broadcast.BroadcastActivity;
import com.example.wfhtasks.implicit.Intents;
import com.example.wfhtasks.inputcontrols.InputActivity;
import com.example.wfhtasks.intents.FirstActivity;
import com.example.wfhtasks.lifecycle.LifeCycle;
import com.example.wfhtasks.notification.NotificationActivity;
import com.example.wfhtasks.roomdb.DbMainActivity;
import com.example.wfhtasks.scrollview.ScrollView;
import com.example.wfhtasks.unitTest.UnitTest;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        Button btnAsync = findViewById( R.id.btn_async );
        Button btnBroadcast = findViewById( R.id.btn_broadcast );
        Button btnImplicit = findViewById( R.id.btn_implicit );
        Button btnInput = findViewById( R.id.btn_input_controls );
        Button btnIntent = findViewById( R.id.btn_intents );
        Button btnLife = findViewById( R.id.btn_life_cycle );
        Button btnNotification = findViewById( R.id.btn_notification );
        Button btnRoom = findViewById( R.id.btn_room );
        Button btnScroll = findViewById( R.id.btn_scroll_view );
        Button btnUnitTest = findViewById( R.id.btn_unit_test );

        btnAsync.setOnClickListener( this );
        btnUnitTest.setOnClickListener( this );
        btnIntent.setOnClickListener( this );
        btnImplicit.setOnClickListener( this );
        btnInput.setOnClickListener( this );
        btnLife.setOnClickListener( this );
        btnNotification.setOnClickListener( this );
        btnRoom.setOnClickListener( this );
        btnScroll.setOnClickListener( this );
        btnBroadcast.setOnClickListener( this );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_async:
                startActivity( new Intent( this, AsyncActivity.class ) );
                break;
            case R.id.btn_broadcast:
                startActivity( new Intent( this, BroadcastActivity.class ) );
                break;
            case R.id.btn_implicit:
                startActivity( new Intent( this, Intents.class ) );
                break;
            case R.id.btn_input_controls:
                startActivity( new Intent( this, InputActivity.class ) );
                break;
            case R.id.btn_intents:
                startActivity( new Intent( this, FirstActivity.class ) );
                break;
            case R.id.btn_life_cycle:
                startActivity( new Intent( this, LifeCycle.class ) );
                break;
            case R.id.btn_notification:
                startActivity( new Intent( this, NotificationActivity.class ) );
                break;
            case R.id.btn_room:
                startActivity( new Intent( this, DbMainActivity.class ) );
                break;
            case R.id.btn_scroll_view:
                startActivity( new Intent( this, ScrollView.class ) );
                break;
            case R.id.btn_unit_test:
                startActivity( new Intent( this, UnitTest.class ) );
                break;

            default:
                Toast.makeText( this, "No such option is there.", Toast.LENGTH_SHORT ).show();
        }
    }
}
