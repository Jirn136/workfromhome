package com.example.wfhtasks.roomdb;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "word_table")
public class WordModel {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "word")
    private String word;

    WordModel(@NonNull String word) {
        this.word = word;
    }

    @NonNull
    String getWord() {
        return word;
    }
}
