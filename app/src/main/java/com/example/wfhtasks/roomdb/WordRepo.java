package com.example.wfhtasks.roomdb;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class WordRepo {
    private Dao mWordDao;
    private LiveData<List<WordModel>> mAllWords;


    WordRepo(Application application) {
        WordDatabase db = WordDatabase.getDatabase(application);
        mWordDao = db.wordDao();
        mAllWords = mWordDao.getWords();
    }


    LiveData<List<WordModel>> getAllWords() {
        return mAllWords;
    }

    public void insert(WordModel word) {
        new insertAsyncTask(mWordDao).execute(word);
    }

    private static class insertAsyncTask extends AsyncTask<WordModel, Void, Void> {

        private Dao mAsyncTaskDao;

        insertAsyncTask(Dao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final WordModel... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
