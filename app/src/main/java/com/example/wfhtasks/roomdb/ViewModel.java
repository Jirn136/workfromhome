package com.example.wfhtasks.roomdb;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class ViewModel extends AndroidViewModel {

    private WordRepo repo;
    private LiveData<List<WordModel>> model;

    public ViewModel(@NonNull Application application) {
        super(application);
        repo = new WordRepo(application);
        model = repo.getAllWords();
    }

    LiveData<List<WordModel>> getModel() {
        return model;
    }

    void insert(WordModel model) {
        repo.insert(model);
    }
}
