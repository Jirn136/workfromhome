package com.example.wfhtasks.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@androidx.room.Dao
public interface Dao {

    @Query("SELECT * from word_table ORDER BY word ASC")
    LiveData<List<WordModel>> getWords();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(WordModel wordModel);

    @Query("DELETE FROM word_table")
    void deleteAll();
}
