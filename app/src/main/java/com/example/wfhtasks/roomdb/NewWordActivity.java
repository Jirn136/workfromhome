package com.example.wfhtasks.roomdb;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.wfhtasks.R;

public class NewWordActivity extends AppCompatActivity {
    public static final String REPLY="Reply message";
    private EditText edtNewWord;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_word);
         edtNewWord= findViewById(R.id.edit_word);
         Button button = findViewById(R.id.button_save);
         button.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent intent= new Intent();
                 if(TextUtils.isEmpty(edtNewWord.getText())) {
                     setResult(RESULT_CANCELED, intent);
                 }else{
                     String word=String.valueOf(edtNewWord.getText());
                     intent.putExtra(REPLY,word);
                     setResult(RESULT_OK,intent);
                 }
                 finish();

             }
         });
    }
}
