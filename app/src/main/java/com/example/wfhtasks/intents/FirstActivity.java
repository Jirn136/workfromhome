package com.example.wfhtasks.intents;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.wfhtasks.R;

public class FirstActivity extends AppCompatActivity {
    private static final String TAG=FirstActivity.class.getSimpleName();
    public static final String MESSAGE="Extra message for simple logic";
    public static final int PASS_REQUEST=1;

    private EditText edtMessage;
    private TextView txtReplyHead;
    private TextView txtReply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        edtMessage=findViewById(R.id.editText_main);
        txtReplyHead=findViewById(R.id.text_header_reply);
        txtReply=findViewById(R.id.text_message_reply);
    }

    public void launchSecondActivity(View view) {
        Log.d(TAG,"launching secondActivity");
        Intent intent= new Intent(this,SecondActivity.class);
        String message= String.valueOf(edtMessage.getText());
        intent.putExtra(MESSAGE,message);
        startActivityForResult(intent,PASS_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==PASS_REQUEST && requestCode==RESULT_OK){
            String reply=data.getStringExtra(SecondActivity.REPLY_MESSAGE);
            txtReplyHead.setVisibility(View.VISIBLE);
            txtReply.setText(reply);
            txtReply.setVisibility(View.VISIBLE);
        }
    }
}
