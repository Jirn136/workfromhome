package com.example.wfhtasks.intents;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.wfhtasks.R;

public class SecondActivity extends AppCompatActivity {
    public static final String REPLY_MESSAGE="Message comes out like this";
    private EditText edtReply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        edtReply=findViewById(R.id.editText_second);
    }

    public void returnReply(View view) {
        String reply=String.valueOf(edtReply.getText());
        Intent intent= new Intent();
        intent.putExtra(REPLY_MESSAGE,reply);
        setResult(RESULT_OK,intent);
        finish();
    }
}
