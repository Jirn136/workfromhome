package com.example.wfhtasks.scrollview;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.wfhtasks.R;

public class ScrollView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroll_view);
    }
}
