package com.example.wfhtasks.asynctask;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.wfhtasks.R;

public class AsyncActivity extends AppCompatActivity {
    private static final String TEXT_STATE = "current text";
    private TextView txtView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async);

        txtView = findViewById(R.id.textView1);
        if (savedInstanceState != null)
            txtView.setText(savedInstanceState.getString(TEXT_STATE));
    }

    public void startTask(View v) {
        txtView.setText(R.string.napping);
        new SimpleAsyncTask(txtView).execute();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TEXT_STATE, String.valueOf(txtView.getText()));
    }
}
