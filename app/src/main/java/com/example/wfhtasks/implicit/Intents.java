package com.example.wfhtasks.implicit;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;

import com.example.wfhtasks.R;

public class Intents extends AppCompatActivity {
    private EditText edtWeb;
    private EditText edtLocation;
    private EditText edtShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intents);
        edtWeb = findViewById(R.id.website_edit_text);
        edtLocation = findViewById(R.id.location_edit_text);
        edtShare = findViewById(R.id.share_edit_text);
    }

    public void openWebsite(View view) {
        String url = String.valueOf(edtWeb.getText());
        Uri webPage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webPage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d("Implicit", "Can't handle");
        }
    }

    public void openLocation(View view) {
        String location = String.valueOf(edtLocation.getText());
        Uri address = Uri.parse(location);
        Intent intent = new Intent(Intent.ACTION_VIEW, address);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d("Implicit", "Can't handle");
        }
    }

    public void shareText(View view) {
        String content = String.valueOf(edtShare.getText());
        String type = "text/plain";
        ShareCompat.IntentBuilder.from(this).setType(type)
                .setChooserTitle(R.string.share_text_with)
                .setText(content).startChooser();
    }
}
